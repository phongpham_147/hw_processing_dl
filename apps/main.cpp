#include "TrafficProcessing.hpp"

#include <opencv2/opencv.hpp>
#include <opencv2/core/ocl.hpp>

const char* keys =
{
    "{ @1             |../data/videos/test_ci.avi  | movie file | }"
	"{ inf inference  |darknet             | Type of inference framework: darknet, ocvdnn | }"
	"{ w weights      |                    | Weights of neural network: yolov4.weights | }"
	"{ c config       |                    | Config file of neural network: yolov4.cfg | }"
	"{ n names        |                    | File with classes names: coco.names | }"
    "{ sf start_frame |0                   | Start a video from this position | }"
    "{ ef end_frame   |0                   | Play a video to this position (if 0 then played to the end of file) | }"
    "{ ed end_delay   |0                   | Delay in milliseconds after video ending | }"
    "{ o  out         |                    | Name of result video file | }"
    "{ sl show_logs   |1                   | Show Trackers logs | }"
	"{ hm heat_map    |0                   | Draw heat map | }"
};

// ----------------------------------------------------------------------

int main(int argc, char** argv)
{
    cv::CommandLineParser parser(argc, argv, keys);

    cv::ocl::setUseOpenCL(1);

    VehicleProcessing vehicle(parser);

	vehicle.AddLine(RoadLine(cv::Point2f(0.1f, 0.6f), cv::Point2f(0.47f, 0.6f), 0));
    vehicle.AddLine(RoadLine(cv::Point2f(0.52f, 0.6f), cv::Point2f(0.9f, 0.6f), 1));

    vehicle.Process();

#ifndef SILENT_WORK
    cv::destroyAllWindows();
#endif

    return 0;
}
