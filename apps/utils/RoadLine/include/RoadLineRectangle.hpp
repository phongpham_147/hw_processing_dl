/**
 * @ Author: Pham Thanh Phong
 * @ Create Time: 2020-10-08 18:09:45
 * @ Modified by: Your name
 * @ Modified time: 2020-10-08 21:12:07
 * @ Description: The resulting detectors are then sitched back together to return bouding box locations
 * with respect to the original scene. To eliminate overlaping bouding box that ocur in overlapping tile scenes,
 * we use an Intersection over Area (IoA) metric.
 * 
 * IoA = Intersection_x + Intersection_y / min(Box1_area,Box2_area)
 * 
 * where:
 * 
 * Intersection_x = min(Box1_xmax,Box2_xmax) - max(Box1_xmin,Box2_xmin)
 * 
 * Intersection_y = min(Box1_ymax, Box2_ymax) - max(Box1_ymin, Box2_ymin)
 * 
 * and box area is
 * 
 * BoxArea = (x_max - y_min)*(y_max - y_min)
 */


#ifndef INTERSECTION_OVER_AREA_H
#define INTERSECTION_OVER_AREA_H

#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>

/** @brief the RoadLineRectangle structure 
 */
class RoadLineRectangle
{
private:

    bool CheckIntersection(cv::Point2f point1, cv::Point2f point2);

public:
    RoadLineRectangle(/* args */);
    RoadLineRectangle(const cv::Point2f &point1, const cv::Point2f &point2,  uint uid);
    ~RoadLineRectangle();

    cv::Point2f m_point1;
    cv::Point2f m_point2;

    uint m_uid = 0;

    int m_intersect1 = 0;
    int m_intersect2 = 0;

    void Draw(cv::Mat frame);
    int isIntersect(cv::Point2f point1, cv::Point2f point2);
};
#endif /* INTERSECTION_OVER_AREA_H */ 