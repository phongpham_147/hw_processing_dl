#include "TrafficProcessing.hpp"

/** @brief TrafficProcessing::TrafficProcessing init input argurment from command line 
 *  \param parser
 */
TrafficProcessing::TrafficProcessing(const cv::CommandLineParser &parser)
	: m_showLogs(true),
	  m_fps(25),
	  m_isTrackerInitialized(false),
	  m_startFrame(0),
	  m_endFrame(0),
	  m_finishDelay(0)
{
	/* std::string pathToModel = "../data/components/yolov4/"; */
	std::string pathToModel = Storage::GetInstance("gs://my-custom-bucket");
	std::string pathOutput = "../data/video/output/";


	//set variable get arguments from console
	m_inFile = parser.get<std::string>(0);
	m_outFile = parser.get<std::string>("out");
	if (m_outFile.empty())
	{
		m_outFile = pathOutput + "_m.avi";
	}
	m_showLogs = parser.get<int>("show_logs") != 0;

	// default path to models storage
	m_modelFile = parser.get<std::string>("weights");
	m_configFile = parser.get<std::string>("config");
	m_classFile = parser.get<std::string>("names");
	if (m_modelFile.empty() && m_configFile.empty())
	{
		m_modelFile = pathToModel + "yolov4.weights";
		m_configFile = pathToModel + "yolov4.cfg";
	}
	if (m_classFile.empty())
		m_classFile = pathToModel + "coco.names";

	m_detectorType = detection::Detectors::TensorRT;

	m_colors.push_back(cv::Scalar(255, 0, 0));
	m_colors.push_back(cv::Scalar(0, 255, 0));
	m_colors.push_back(cv::Scalar(0, 0, 255));
	m_colors.push_back(cv::Scalar(255, 255, 0));
	m_colors.push_back(cv::Scalar(0, 255, 255));
	m_colors.push_back(cv::Scalar(255, 0, 255));
	m_colors.push_back(cv::Scalar(255, 127, 255));
	m_colors.push_back(cv::Scalar(127, 0, 255));
	m_colors.push_back(cv::Scalar(127, 0, 127));
}

TrafficProcessing::~TrafficProcessing()
{
}

/** @brief TrafficProcessing::Process 
  Call all process algorithm follow pipeline |Video|->|Capture|->|Detector|->|Tracker|->|Velocity|->|Visualize|
 */
void TrafficProcessing::Process()
{
	cv::VideoWriter writer;

	int flag = 0;

	/** @brief function bellow will help we measurement performance with opencv execuation project in time (second)
	 ** inferences: https://docs.opencv.org/master/dc/d71/tutorial_py_optimization.html
	 ** ex:
	e1 = cv.getTickCount()
	# your code execution
	e2 = cv.getTickCount()
	time = (e2 - e1)/ cv.getTickFrequency()
	 */
	double freq = cv::getTickFrequency();

	int64 allTime = 0;

	bool manualMode = false;
	int framesCounter = m_startFrame + 1;

	cv::VideoCapture capture;
	if (!isOpenCapture(capture))
	{
		return;
	}
	// check capture has read frame from video
	cv::Mat frame;
	capture >> frame;
	if (frame.empty())
	{
		std::cerr << "Frame is empty!" << std::endl;
		return;
	}

	if (!isTrackerInitialized(frame))
	{
		return;
	}

	for (;;)
	{
		capture >> frame;
		if (frame.empty())
		{
			std::cerr << "Frame is empty!" << std::endl;
			break;
		}

		//get time start processing program
		int64 t1 = cv::getTickCount();

		// implemented using CPU or OpenCL
		cv::UMat uframe;
		if (!m_detector->CanGrayProcessing() || m_tracker->CanColorFrameToTrack())
			uframe = frame.getUMat(cv::ACCESS_READ);
		else
			cv::cvtColor(frame, uframe, cv::COLOR_BGR2GRAY);

		m_detector->Detect(uframe);

		const regions_t &regions = m_detector->GetDetects();

		m_tracker->Update(regions, uframe, m_fps);

		//get time end processing program
		int64 t2 = cv::getTickCount();

		allTime += t2 - t1;
		int currTime = cvRound(1000 * (t2 - t1) / freq);

		DrawData(frame, framesCounter, currTime);

#ifndef SILENT_WORKV
		//monitor visualize result processing
		cv::imshow("Video", frame);

		/** @brief visualize manual mode support working debug processing 
		 * toggle manual mode
		 */
		int waitTime = manualMode ? 0 : 1;
		std::stringstream label;
		if (waitTime)
		{
			label << "ON";
		}
		else
		{
			label << "OFF";
		}
		cv::putText(frame, "Manual mode: " + label.str(), frame.size(), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255, 255, 255), 2);

		flag = cv::waitKey(waitTime);

		/* 
		* set toggle for using change mode debug
		*/
		if (flag == 'm' || flag == 'M')
			manualMode = !manualMode;
		if (flag == 27) // wait for 'esc' key press for 30ms. If 'esc' key is pressed, break loop
			break;

#else
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
#endif

		if (!m_outFile.empty() && !writer.isOpened())
			writer.open(m_outFile, cv::VideoWriter::fourcc('M', 'J', 'P', 'G'), m_fps, frame.size(), true);
		if (writer.isOpened())
			writer << frame;

		++framesCounter;
		if (m_endFrame && framesCounter > m_endFrame)
		{
			std::cout << "Process: riched last " << m_endFrame << " frame" << std::endl;
			break;
		}
	}

	std::cout << "work time = " << (allTime / freq) << std::endl;
#ifndef SILENT_WORK
	cv::waitKey(m_finishDelay);
#endif
}

/// 
/// \brief TrafficProcessing::DrawTrack
/// \param frame
/// \param resizeCoeff
/// \param track
/// \param drawTrajectory
///
void TrafficProcessing::DrawTrack(cv::Mat frame,
								  int resizeCoeff,
								  const TrackingObject &track,
								  bool drawTrajectory)
{
	auto ResizeRect = [&](const cv::Rect &r) -> cv::Rect {
		return cv::Rect(resizeCoeff * r.x, resizeCoeff * r.y, resizeCoeff * r.width, resizeCoeff * r.height);
	};
	auto ResizePoint = [&](const cv::Point &pt) -> cv::Point {
		return cv::Point(resizeCoeff * pt.x, resizeCoeff * pt.y);
	};

	cv::Rect box_rect = track.m_rrect.boundingRect();
	if (track.m_isStatic)
	{
#if (CV_VERSION_MAJOR >= 4)
		cv::rectangle(frame, ResizeRect(box_rect), cv::Scalar(255, 0, 255), 2, cv::LINE_AA);
#else
		cv::rectangle(frame, ResizeRect(box_rect), cv::Scalar(255, 0, 255), 2, CV_AA);
#endif
	}
	else
	{
#if (CV_VERSION_MAJOR >= 4)
		cv::rectangle(frame, ResizeRect(box_rect), cv::Scalar(0, 255, 0), 1, cv::LINE_AA);
#else
		cv::rectangle(frame, ResizeRect(track.m_rrect.boundingRect()), cv::Scalar(0, 255, 0), 1, CV_AA);
#endif
		if (!m_geoParams.Empty())
		{
			int traceSize = static_cast<int>(track.m_trace.size());
			int period = std::min(2 * cvRound(m_fps), traceSize);
			const auto &from = m_geoParams.Pix2Geo(track.m_trace[traceSize - period]);
			const auto &to = m_geoParams.Pix2Geo(track.m_trace[traceSize - 1]);
			auto dist = DistanceInMeters(from, to);

			std::stringstream label;
			std::cout << "FPS" << m_fps << std::endl;
			if (period >= cvRound(m_fps) / 4)
			{
				auto velocity = (3.6f * dist * m_fps) / period;

				std::cout << track.m_type << ": distance " << std::fixed << std::setw(2) << std::setprecision(2) << dist << " on time " << (period / m_fps) << " with velocity " << velocity << " km/h: " << track.m_confidence << std::endl;

				if (velocity < 1.f || std::isnan(velocity))
				{
					velocity = 0;
				}

				label << "ID: " << track.m_ID << "\n"
					  << cvRound(velocity) << " km/h";

				int baseLine = 0;
				float fontScale = 0.2;
				cv::Size labelSize = cv::getTextSize(label.str(), cv::FONT_HERSHEY_SIMPLEX, fontScale, 1, &baseLine);

				if (box_rect.x < 0)
				{
					box_rect.width = std::min(box_rect.width, frame.cols - 1);
					box_rect.x = 0;
				}
				else if (box_rect.x + box_rect.width >= frame.cols)
				{
					box_rect.x = std::max(0, frame.cols - box_rect.width - 1);
					box_rect.width = std::min(box_rect.width, frame.cols - 1);
				}
				if (box_rect.y - labelSize.height < 0)
				{
					box_rect.height = std::min(box_rect.height, frame.rows - 1);
					box_rect.y = labelSize.height;
				}
				else if (box_rect.y + box_rect.height >= frame.rows)
				{
					box_rect.y = std::max(0, frame.rows - box_rect.height - 1);
					box_rect.height = std::min(box_rect.height, frame.rows - 1);
				}

				if (velocity < 70)
				{
					cv::rectangle(frame, cv::Rect(cv::Point(box_rect.x, box_rect.y - labelSize.height), cv::Size(labelSize.width, labelSize.height + baseLine)), cv::Scalar(0, 255, 0), cv::FILLED);
				}
				else
				{
					cv::rectangle(frame, cv::Rect(cv::Point(box_rect.x, box_rect.y - labelSize.height), cv::Size(labelSize.width, labelSize.height + baseLine)), cv::Scalar(0, 0, 255), cv::FILLED);
					//cv::rectangle(frame, ResizeRect(track.m_rrect.boundingRect()), cv::Scalar(0, 0, 0), 1, cv::LINE_AA);
				}
				cv::putText(frame, label.str(), box_rect.tl(), cv::FONT_HERSHEY_SIMPLEX, fontScale, cv::Scalar(0, 0, 0));
			}
		}
	}

	if (drawTrajectory)
	{
		cv::Scalar cl = m_colors[track.m_ID % m_colors.size()];

		for (size_t j = 0; j < track.m_trace.size() - 1; ++j)
		{
			const TrajectoryPoint &pt1 = track.m_trace.at(j);
			const TrajectoryPoint &pt2 = track.m_trace.at(j + 1);
#if (CV_VERSION_MAJOR >= 4)
			cv::line(frame, ResizePoint(pt1.m_prediction), ResizePoint(pt2.m_prediction), cl, 1, cv::LINE_AA);
#else
			cv::line(frame, ResizePoint(pt1.m_prediction), ResizePoint(pt2.m_prediction), cl, 1, CV_AA);
#endif
			if (!pt2.m_hasRaw)
			{
#if (CV_VERSION_MAJOR >= 4)
				cv::circle(frame, ResizePoint(pt2.m_prediction), 4, cl, 1, cv::LINE_AA);
#else
				cv::circle(frame, ResizePoint(pt2.m_prediction), 4, cl, 1, CV_AA);
#endif
			}
		}
	}
}

///
/// \brief TrafficProcessing::InitTracker
/// \param grayFrame
///
bool TrafficProcessing::InitTracker(cv::UMat frame)
{
	config_t config;

	bool res = true;

	m_minObjWidth = frame.cols / 50;

	const int minStaticTime = 5;

	/* 
	 * cofig parameter using DNN
	 */
	std::cout << "Configuration paramater for DNN" << std::endl;
	config.emplace("modelWeights", m_modelFile);
	config.emplace("modelConfig", m_configFile);
	config.emplace("classNames", m_classFile);
	config.emplace("confidenceThreshold", m_confThresh);
	config.emplace("NMSThreshold", m_nmsThresh);
	config.emplace("swapRB", m_swapRB);
	config.emplace("maxAspectRaito", m_maxAspectRatio);

	m_detector = std::unique_ptr<BaseDetector>(CreateDetector(detection::Darknet, config, frame));

	if (m_detector.get())
		m_detector->SetMinObjectSize(cv::Size(m_minObjWidth, m_minObjWidth));
	else
		res = false;

	if (res)
	{
		TrackerSettings settings;
		settings.SetDistance(tracking::DistanceJaccard);
		settings.m_kalmanType = tracking::KalmanLinear;
		settings.m_filterGoal = tracking::FilterCenter;
		settings.m_lostTrackType = tracking::TrackCSRT; // Use KCF tracker for collisions resolving
		settings.m_matchType = tracking::MatchHungrian;
		settings.m_dateTime = 0.3f;			 // Delta time for Kalman filter
		settings.m_accelNoiseMagnitude = 0.2f; // Accel noise magnitude for Kalman filter
		settings.m_distanceThreshold = 0.7f;	 // Distance threshold between region and object on two frames
		settings.m_minAreaRadiusPix = frame.rows / 20.f;
		settings.m_maximumAllowedSkippedFrames = cvRound(2 * m_fps); // Maximum allowed skipped frames

		settings.AddNearTypes("car", "bus", false);
		settings.AddNearTypes("car", "truck", false);
		settings.AddNearTypes("person", "bicycle", true);
		settings.AddNearTypes("person", "motorbike", true);

		settings.m_useAbandonedDetection = false;
		if (settings.m_useAbandonedDetection)
		{
			settings.m_minStaticTime = minStaticTime;
			settings.m_maxStaticTime = 60;
			settings.m_maximumAllowedSkippedFrames = cvRound(settings.m_minStaticTime * m_fps); // Maximum allowed skipped frames
			settings.m_maxTraceLength = 2 * settings.m_maximumAllowedSkippedFrames;				// Maximum trace length
		}
		else
		{
			settings.m_maximumAllowedSkippedFrames = cvRound(10 * m_fps); // Maximum allowed skipped frames
			settings.m_maxTraceLength = cvRound(4 * m_fps);				  // Maximum trace length
		}

		m_tracker = std::make_unique<CTracker>(settings);
	}

#if 1
#if 1
	std::vector<cv::Point> framePoints{cv::Point(420, 348), cv::Point(509, 283), cv::Point(731, 281), cv::Point(840, 343)};
	std::vector<cv::Point2f> geoPoints{cv::Point2f(45.526646, 5.974535), cv::Point2f(45.527566, 5.973849), cv::Point2f(45.527904, 5.974135), cv::Point2f(45.526867, 5.974826)};
#else
	std::vector<cv::Point> framePoints{cv::Point(1665, 746), cv::Point(246, 521), cv::Point(570, 282), cv::Point(1773, 378)};
	std::vector<cv::Point2f> geoPoints{cv::Point2f(30.258855, 60.006536), cv::Point2f(30.258051, 60.006855), cv::Point2f(30.258080, 60.007414), cv::Point2f(30.259066, 60.007064)};
#endif
	m_geoParams.SetKeyPoints(framePoints, geoPoints);
#endif

	return res;
}

///
/// \brief TrafficProcessing::DrawData
/// \param frame
///Center
void TrafficProcessing::DrawData(cv::Mat frame, int framesCounter, int currTime)
{
	auto tracks = m_tracker->GetTracks();

	if (m_showLogs)
	{
		std::cout << "Frame " << framesCounter << ": tracks = " << tracks.size() << ", time = " << currTime << std::endl;
	}

	if (!m_geoParams.Empty())
	{
		std::vector<cv::Point> points = m_geoParams.GetFramePoints();
		for (size_t i = 0; i < points.size(); ++i)
		{
			cv::line(frame, points[i % points.size()], points[(i + 1) % points.size()], cv::Scalar(255, 255, 255), 1, cv::LINE_AA);
		}
	}

	for (const auto &track : tracks)
	{
		if (track.m_isStatic)
		{
			DrawTrack(frame, 1, track, true);
		}
		else
		{
			if (track.IsRobust(cvRound(m_fps / 4),	   // Minimal trajectory size
							   0.8f,				   // Minimal ratio raw_trajectory_points / trajectory_lenght
							   cv::Size2f(0.1f, 8.0f)) // Min and max ratio: width / height
			)
			{
				DrawTrack(frame, 1, track, true);

				CheckLinesIntersection(track, static_cast<float>(frame.cols), static_cast<float>(frame.rows));
			}
		}
	}
	//m_detector->CalcMotionMap(frame);

	for (const auto &rl : m_lines)
	{
		rl.Draw(frame);
	}
}

///
/// \brief TrafficProcessing::AddLine
/// \param newLine
///
void TrafficProcessing::AddLine(const RoadLine &newLine)
{
	m_lines.push_back(newLine);
}

///
/// \brief TrafficProcessing::GetLine
/// \param lineUid
/// \return
///
bool TrafficProcessing::GetLine(unsigned int lineUid, RoadLine &line)
{
	for (const auto &rl : m_lines)
	{
		if (rl.m_uid == lineUid)
		{
			line = rl;
			return true;
		}
	}
	return false;
}

///
/// \brief TrafficProcessing::RemoveLine
/// \param lineUid
/// \return
///
bool TrafficProcessing::RemoveLine(unsigned int lineUid)
{
	for (auto it = std::begin(m_lines); it != std::end(m_lines);)
	{
		if (it->m_uid == lineUid)
			it = m_lines.erase(it);
		else
			++it;
	}
	return false;
}

///
/// \brief TrafficProcessing::CheckLinesIntersection
/// \param track
///
void TrafficProcessing::CheckLinesIntersection(const TrackingObject &track, float xMax, float yMax)
{
	auto Pti2f = [&](cv::Point pt) -> cv::Point2f {
		return cv::Point2f(pt.x / xMax, pt.y / yMax);
	};

	constexpr size_t minTrack = 5;
	if (track.m_trace.size() >= minTrack)
	{
		for (auto &rl : m_lines)
		{
			if (m_lastIntersections.find(track.m_ID) == m_lastIntersections.end())
			{
				if (rl.IsIntersect(Pti2f(track.m_trace[track.m_trace.size() - minTrack]), Pti2f(track.m_trace[track.m_trace.size() - 1])))
					m_lastIntersections.emplace(track.m_ID);
			}
		}
	}
}

/** @brief TrafficProcessing::Detection call all object detect from libs  
 * frame
 * regions
 */
void TrafficProcessing::Detection(cv::Mat frame, regions_t &regions)
{
	cv::UMat uframe;
	if (!m_detector->CanGrayProcessing())
	{
		uframe = frame.getUMat(cv::ACCESS_READ);
	}
	else
	{
		cv::cvtColor(uframe, frame, cv::COLOR_BGR2GRAY);
	}

	m_detector->Detect(uframe);

	const regions_t &regions = m_detector->GetDetects();

	regions.assign(std::begin(regions), std::end(regions));
}

void TrafficProcessing::Tracking(cv::Mat frame, regions_t &regions)
{
	cv::UMat uframe;
	if (m_tracker->CanColorFrameToTrack())
	{
		uframe = frame.getUMat(cv::ACCESS_READ);
	}
	else
	{
		cv::cvtColor(uframe, frame, cv::COLOR_BGR2GRAY);
	}

	m_tracker->Update(regions, uframe, m_fps);
}

/** @brief TrafficProcessing::isOpenCapture check can open file is input algorihtm
 * capture 
 * return
 */
bool TrafficProcessing::isOpenCapture(cv::VideoCapture &capture)
{
	if (m_inFile.size() == 1)
	{
		capture.open(atoi(m_inFile.c_str()));
	}
	else
	{
		capture.open(m_inFile);
	}

	if (capture.isOpened())
	{
		capture.set(cv::CAP_PROP_POS_FRAMES, 0);
		m_fps = std::max(1.f, (float)capture.get(cv::CAP_PROP_FPS));
		return true;
	}
	return false;
}

/** @brief TrafficProcessing::isTrackerInitialized If tracker uninitialized then function try init 
 * frame
 * return
 */
bool TrafficProcessing::isTrackerInitialized(cv::Mat frame)
{
	if (!m_isTrackerInitialized)
	{
		cv::UMat uframe = frame.getUMat(cv::ACCESS_READ);
		m_isTrackerInitialized = InitTracker(uframe);
		if (!m_isTrackerInitialized)
		{
			return false;
		}
	}

	return true;

}

bool TrafficProcessing::isOpenCapture(cv::VideoCapture &capture)
{
	if(capture.open(m_inFile))
	{
		return true;
	}
	return false;
}