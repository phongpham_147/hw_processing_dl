![travis ci:](https://travis-ci.org/Smorodov/Multitarget-tracker.svg?branch=master)

# Description
The developer board. NVIDIA's Jetson TX2, is what runs the DNN algorithm and combine detween the framework YoLoV4 and TensorRT of NIVIDA open-source is we using into project. Researching and calculator the popular math instance Kalman Filter, Hungarian, Insection over Union, Insection over Area, etc. Data is captured using 2 - 8 module SONY-CAMU05 camera. The camera is use powered the Jetson TX2 communicates using MIPI camera serial interface specification. Data will transmission follow two step is save onto an SD card and send to cloud (Google Cloud Platform).

# Phase develope project

**Phase 1** : Development software inference algorithm processing AI 

**Phase 2** : Developement docker image for project.

**Phase 3** : Embedded software into kernel Linux of Jetson TX2

**Phase 4** : Testing input video into Jetson TX2 and Accuracy

**Phase 5** : Testing project in life on traffic at Viet Nam


# Build porject

This is project will use [Google Cloud](https://cloud.google.com/gcp/?utm_sgitource=google&utm_medium=cpc&utm_campaign=japac-VN-all-en-dr-bkws-all-super-trial-e-dr-1009137&utm_content=text-ad-none-none-DEV_c-CRE_450509447652-ADGP_Hybrid+%7C+AW+SEM+%7C+BKWS+~+T1+%7C+EXA+%7C+General+%7C+1:1+%7C+VN+%7C+en+%7C+google+cloud-KWID_43700023274810597-kwd-296530647816&userloc_1028581-network_g&utm_term=KW_google%20cloud&gclid=CjwKCAjwh7H7BRBBEiwAPXjadlVNCNDlWycF2TBK6_sTHPDCJzW3vtONssCGT0kPkX3Dg0z_LBZCexoCMqEQAvD_BwE) for build and testing software run on CUDNN 7.6.2 and Cuda 10.2 

Run cml on terminal
```
${PATH_FOLDER}/.build.sh
```
# Install proccess
Run cml on terminal
```
${PATH_FOLDER}/.install
```
## Uasge
```
       1. [Optional] Name of result video file
          -o=out.avi or --out=result.mp4
       2. [Optional] Show Trackers logs in terminal
          -sl=1 or --show_logs=0
       3. [Optional] Use built-in OpenCL
          -g=1 or --gpu=0
       4. [Optional] Use 2 threads for processing pipeline
          -a=1 or --async=0
```

## Package Libs
We design BaseDetector is similar as a Proxy tech. 
Using proxy is call `bd::<namespace>`
```cpp
#ifndef BASE_DETECTOR_H
#define BASE_DETECTOR_H

#include <memory>
#include "../../common/defines.h"

///
/// \brief The BaseDetector class
///
namespace base 
{
class BaseDetector
{
public:
    ///
    /// \brief BaseDetector
    /// \param frame
    ///
	BaseDetector(const cv::UMat& frame)
	{
		m_minObjectSize.width = std::max(5, frame.cols / 100);
		m_minObjectSize.height = m_minObjectSize.width;
    }
    ///
    /// \brief ~BaseDetector
    ///
    virtual ~BaseDetector(void)
    {
    }

    ///
    /// \brief Init
    /// \param config
    ///
    virtual bool Init(const config_t& config) = 0;

    ///
    /// \brief Detect
    /// \param frame
    ///
    virtual void Detect(const cv::UMat& frame) = 0;

	///
	/// \brief ResetModel
	/// \param img
	/// \param roiRect
	///
	virtual void ResetModel(const cv::UMat& /*img*/, const cv::Rect& /*roiRect*/)
	{
	}

	///
	/// \brief CanGrayProcessing
	///
	virtual bool CanGrayProcessing() const = 0;

    ///
    /// \brief SetMinObjectSize
    /// \param minObjectSize
    ///
    void SetMinObjectSize(cv::Size minObjectSize)
    {
        m_minObjectSize = minObjectSize;
    }

    ///
    /// \brief GetDetects
    /// \return
    ///
    const regions_t& GetDetects() const
    {
        return m_regions;
    }

    ///
    /// \brief CalcMotionMap
    /// \param frame
    ///
    virtual void CalcMotionMap(cv::Mat& frame)
    {
        if (m_motionMap.size() != frame.size())
            m_motionMap = cv::Mat(frame.size(), CV_32FC1, cv::Scalar(0, 0, 0));

        cv::Mat foreground(m_motionMap.size(), CV_8UC1, cv::Scalar(0, 0, 0));
        for (const auto& region : m_regions)
        {
#if (CV_VERSION_MAJOR < 4)
            cv::ellipse(foreground, region.m_rrect, cv::Scalar(255, 255, 255), CV_FILLED);
#else
            cv::ellipse(foreground, region.m_rrect, cv::Scalar(255, 255, 255), cv::FILLED);
#endif
        }

        cv::normalize(foreground, m_normFor, 255, 0, cv::NORM_MINMAX, m_motionMap.type());

        double alpha = 0.95;
        cv::addWeighted(m_motionMap, alpha, m_normFor, 1 - alpha, 0, m_motionMap);

        const int chans = frame.channels();
		const int height = frame.rows;
#pragma omp parallel for
        for (int y = 0; y < height; ++y)
        {
            uchar* imgPtr = frame.ptr(y);
            const float* moPtr = reinterpret_cast<float*>(m_motionMap.ptr(y));
            for (int x = 0; x < frame.cols; ++x)
            {
                for (int ci = chans - 1; ci < chans; ++ci)
                {
                    imgPtr[ci] = cv::saturate_cast<uchar>(imgPtr[ci] + moPtr[0]);
                }
                imgPtr += chans;
                ++moPtr;
            }
        }
    }

protected:
    regions_t m_regions;

    cv::Size m_minObjectSize;

	// Motion map for visualization current detections
    cv::Mat m_motionMap;
	cv::Mat m_normFor;

	///
    std::vector<cv::Rect> GetCrops(float maxCropRatio, cv::Size netSize, cv::Size imgSize) const
    {
        std::vector<cv::Rect> crops;

        const float whRatio = static_cast<float>(netSize.width) / static_cast<float>(netSize.height);
        int cropHeight = cvRound(maxCropRatio * netSize.height);
        int cropWidth = cvRound(maxCropRatio * netSize.width);

        if (imgSize.width / (float)imgSize.height > whRatio)
        {
            if (cropHeight >= imgSize.height)
                cropHeight = imgSize.height;
            cropWidth = cvRound(cropHeight * whRatio);
        }
        else
        {
            if (cropWidth >= imgSize.width)
                cropWidth = imgSize.width;
            cropHeight = cvRound(cropWidth / whRatio);
        }

        //std::cout << "Frame size " << imgSize << ", crop size = " << cv::Size(cropWidth, cropHeight) << ", ratio = " << maxCropRatio << std::endl;

        const int stepX = 3 * cropWidth / 4;
        const int stepY = 3 * cropHeight / 4;
        for (int y = 0; y < imgSize.height; y += stepY)
        {
            bool needBreakY = false;
            if (y + cropHeight >= imgSize.height)
            {
                y = imgSize.height - cropHeight;
                needBreakY = true;
            }
            for (int x = 0; x < imgSize.width; x += stepX)
            {
                bool needBreakX = false;
                if (x + cropWidth >= imgSize.width)
                {
                    x = imgSize.width - cropWidth;
                    needBreakX = true;
                }
                crops.emplace_back(x, y, cropWidth, cropHeight);
                if (needBreakX)
                    break;
            }
            if (needBreakY)
                break;
        }
        return crops;
    };
};


///
/// \brief CreateDetector
/// \param detectorType .enumerate {Yolo_Darknet, Yolo_TensorRT}
/// \param gray get frame is capture from process
/// \return
///
BaseDetector* CreateDetector(tracking::Detectors detectorType, const config_t& config, cv::UMat& gray);

};

#endif /* BASE_DETECTOR_H */

```

## Environment build programming
We are use `ninja-build` for increase performance building for project 
```
Ninja is a small build system with a focus on speed. It differs from other build systems in two major respects: it is designed to have its input files generated by a higher-level build system, and it is designed to run builds as fast as possible.
```

#### Thirdparty libraries
* OpenCV (and contrib): https://github.com/opencv/opencv and https://github.com/opencv/opencv_contrib
* Vibe: https://github.com/BelBES/VIBE
* SuBSENSE and LOBSTER: https://github.com/ethereon/subsense
* GTL: https://github.com/rdmpage/graph-template-library
* MWBM: https://github.com/rdmpage/maximum-weighted-bipartite-matching
* Pedestrians detector: https://github.com/sturkmen72/C4-Real-time-pedestrian-detection
* Non Maximum Suppression: https://github.com/Nuzhny007/Non-Maximum-Suppression
* MobileNet SSD models: https://github.com/chuanqi305/MobileNet-SSD
* YOLO v3 models: https://pjreddie.com/darknet/yolo/
* Darknet inference and YOLO v4 models: https://github.com/AlexeyAB/darknet
* NVidia TensorRT inference and YOLO v5 models: https://github.com/enazoe/yolo-tensorrt
* GOTURN models: https://github.com/opencv/opencv_extra/tree/c4219d5eb3105ed8e634278fad312a1a8d2c182d/testdata/tracking
* DAT tracker: https://github.com/foolwood/DAT
* STAPLE tracker: https://github.com/xuduo35/STAPLE
* LDES tracker: https://github.com/yfji/LDESCpp

#### License
GNU GPLv3: http://www.gnu.org/licenses/gpl-3.0.txt 

#### Project cititations
1. Jeroen PROVOOST "Camera gebaseerde analysevan de verkeersstromen aaneen kruispunt", 2014 ( https://iiw.kuleuven.be/onderzoek/eavise/mastertheses/provoost.pdf )
2. Roberto Ciano, Dimitrij Klesev "Autonome Roboterschwarme in geschlossenen Raumen", 2015 ( https://www.hs-furtwangen.de/fileadmin/user_upload/fak_IN/Dokumente/Forschung_InformatikJournal/informatikJournal_2016.pdf#page=18 )
3. Wenda Qin, Tian Zhang, Junhe Chen "Traffic Monitoring By Video: Vehicles Tracking and Vehicle Data Analysing", 2016 ( http://cs-people.bu.edu/wdqin/FinalProject/CS585%20FinalProjectReport.html )
4. Ipek BARIS "CLASSIFICATION AND TRACKING OF VEHICLES WITH HYBRID CAMERA SYSTEMS", 2016 ( http://cvrg.iyte.edu.tr/publications/IpekBaris_MScThesis.pdf )
5. Cheng-Ta Lee, Albert Y. Chen, Cheng-Yi Chang "In-building Coverage of Automated External Defibrillators Considering Pedestrian Flow", 2016 ( http://www.see.eng.osaka-u.ac.jp/seeit/icccbe2016/Proceedings/Full_Papers/092-132.pdf )
6. Roberto Ciano, Dimitrij Klesev "Autonome Roboterschwarme in geschlossenen Raumen" in "informatikJournal 2016/17", 2017 ( https://docplayer.org/124538994-2016-17-informatikjournal-2016-17-aktuelle-berichte-aus-forschung-und-lehre-der-fakultaet-informatik.html )
7. Omid Noorshams "Automated systems to assess weights and activity in grouphoused mice", 2017 ( https://pdfs.semanticscholar.org/e5ff/f04b4200c149fb39d56f171ba7056ab798d3.pdf ) 
8. RADEK VOPÁLENSKÝ "DETECTION,TRACKING AND CLASSIFICATION OF VEHICLES", 2018 ( https://www.vutbr.cz/www_base/zav_prace_soubor_verejne.php?file_id=181063 )
9. Márk Rátosi, Gyula Simon "Real-Time Localization and Tracking  using Visible Light Communication", 2018 ( https://ieeexplore.ieee.org/abstract/document/8533800 )
10. Thi Nha Ngo, Kung-Chin Wu, En-Cheng Yang, Ta-Te Lin "Areal-time imaging system for multiple honey bee tracking and activity monitoring", 2019 ( https://www.sciencedirect.com/science/article/pii/S0168169919301498 )
11. ROS, http://docs.ros.org/lunar/api/costmap_converter/html/Ctracker_8cpp_source.html
