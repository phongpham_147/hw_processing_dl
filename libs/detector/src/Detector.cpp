/** 
 *  @file   IGraphicsContext.hpp 
 *  @brief  This is super class select using the algorithm is Darknet and TensorRT
 *  @author Somebody else 
 *  @date   2012-02-20 
 ***********************************************/

#include "Detector.hpp"

#ifdef BUILD_DARKNET
#include "DarknetDetector.hpp"
#endif
#ifdef BUILD_TENSORRT
#include "TensorRTDetector.hpp"
#endif
///
/// \brief CreateDetector
/// \param detectorType
/// \param gray
/// \return
///
BaseDetector *CreateDetector(detection::Detectors detectorType, const config_t &config, cv::UMat &frame)
{
    BaseDetector *detector = nullptr;

    switch (detectorType)
    {
    case detection::Darknet:
#ifdef BUILD_DARKNET
        detector = new DarknetDetector(frame);
#else
        std::cerr << "Darknet inferences isn't configured in CMake" << std::endl;
#endif
        break;

    case detection ::TensorRT:
#ifdef BUILD_TENSORRT
        detector = new TensorRTDetector(frame);
#else
        std::cerr << "TensorRT inference engine isn't configured in CMake" << std::endl;
#endif
        break;

    default:
        break;
    }

    if (!detector->Init(config))
    {
        delete detector;
        detector = nullptr;
    }
    return detector;
}
