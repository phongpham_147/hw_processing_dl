/** 
 *  @file   BaseDetector.h
 *  @brief  Class BaseDetector this similar as a Proxy provides a authenticated for project. We can using it as connect API C++ Detector with applications. 
 *  @author Pham Thanh Phong 
 *  @date   2020-10-10 
 ***********************************************/

#ifndef BASE_DETECTOR_H
#define BASE_DETECTOR_H

#include <memory>
#include "../../common/defines.hpp"

/** 
 * \brief The BaseDetector class is Supper Class provider for application the algorithm detector from the branch inheritance
 */
class BaseDetector
{
public:
    /** 
     * \brief Call contructor will calculator square of frame
     * \param frame 
     */
    BaseDetector(const cv::UMat &frame)
    {
        m_minObjectSize.width = std::max(5, frame.cols / 100);
        m_minObjectSize.height = m_minObjectSize.width;
    }

    /**
     * \brief ~BaseDetector decontructor remove memory cache when appliication authencated.  
     */
    virtual ~BaseDetector(void)
    {
    }

    /**
     * \brief Check whether models training have is intialize and request developer must process this function
     * \param config
     */
    virtual bool Init(const config_t &config) = 0;

	/** 
	 * \brief Sets the the select when paramater maxCroppRaito less than 0 then function will call process detector with frame original.
	 * Esle then call function detector in crop with paramater crop ratio. 
	 * \param colorFrame metric image
	 */
    virtual void Detect(const cv::UMat &frame) = 0;

    /** 
     * \brief CanGrayProcessing 
     */
    virtual bool CanGrayProcessing() const = 0;

    /** 
     * \brief SetMinObjectSize provide select object minimum can detector.
     * \param minObjectSize
     */
    void SetMinObjectSize(cv::Size minObjectSize)
    {
        m_minObjectSize = minObjectSize;
    }

    /**
     * \brief GetDetects provide constant the object detector  
     */
    const regions_t &GetDetects() const
    {
        return m_regions;
    }
    
    /** 
     * \brief CalculatorMotionMap call by preference 
     * \param frame
     */
    virtual void CalculatorMotionMap(cv::Mat &frame)
    {
        if (m_motionMap.size() != frame.size())
            m_motionMap = cv::Mat(frame.size(), CV_32FC1, cv::Scalar(0, 0, 0));

        cv::Mat foreground(m_motionMap.size(), CV_8UC1, cv::Scalar(0, 0, 0));
        for (const auto &region : m_regions)
        {
#if (CV_VERSION_MAJOR < 4)
            cv::ellipse(foreground, region.m_rrect, cv::Scalar(255, 255, 255), CV_FILLED);
#else
            cv::ellipse(foreground, region.m_rrect, cv::Scalar(255, 255, 255), cv::FILLED);
#endif
        }

        cv::normalize(foreground, m_normFor, 255, 0, cv::NORM_MINMAX, m_motionMap.type());

        double alpha = 0.95;
        cv::addWeighted(m_motionMap, alpha, m_normFor, 1 - alpha, 0, m_motionMap);

        const int chans = frame.channels();
        const int height = frame.rows;
#pragma omp parallel for
        for (int y = 0; y < height; ++y)
        {
            uchar *imgPtr = frame.ptr(y);
            const float *moPtr = reinterpret_cast<float *>(m_motionMap.ptr(y));
            for (int x = 0; x < frame.cols; ++x)
            {
                for (int ci = chans - 1; ci < chans; ++ci)
                {
                    imgPtr[ci] = cv::saturate_cast<uchar>(imgPtr[ci] + moPtr[0]);
                }
                imgPtr += chans;
                ++moPtr;
            }
        }
    }

protected:
    regions_t m_regions;

    cv::Size m_minObjectSize;

    // Motion map for visualization current detections
    cv::Mat m_motionMap;
    cv::Mat m_normFor;

    /* 
     * https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4721758/pdf/sensors-15-29838.pdf
     */
    std::vector<cv::Rect> GetCrops(float maxCropRatio, cv::Size netSize, cv::Size imgSize) const
    {
        std::vector<cv::Rect> crops;

        const float whRatio = static_cast<float>(netSize.width) / static_cast<float>(netSize.height);
        int cropHeight = cvRound(maxCropRatio * netSize.height);
        int cropWidth = cvRound(maxCropRatio * netSize.width);

        if (imgSize.width / (float)imgSize.height > whRatio)
        {
            if (cropHeight >= imgSize.height)
                cropHeight = imgSize.height;
            cropWidth = cvRound(cropHeight * whRatio);
        }
        else
        {
            if (cropWidth >= imgSize.width)
                cropWidth = imgSize.width;
            cropHeight = cvRound(cropWidth / whRatio);
        }

        //std::cout << "Frame size " << imgSize << ", crop size = " << cv::Size(cropWidth, cropHeight) << ", ratio = " << maxCropRatio << std::endl;

        const int stepX = 3 * cropWidth / 4;
        const int stepY = 3 * cropHeight / 4;
        for (int y = 0; y < imgSize.height; y += stepY)
        {
            bool needBreakY = false;
            if (y + cropHeight >= imgSize.height)
            {
                y = imgSize.height - cropHeight;
                needBreakY = true;
            }
            for (int x = 0; x < imgSize.width; x += stepX)
            {
                bool needBreakX = false;
                if (x + cropWidth >= imgSize.width)
                {
                    x = imgSize.width - cropWidth;
                    needBreakX = true;
                }
                crops.emplace_back(x, y, cropWidth, cropHeight);
                if (needBreakX)
                    break;
            }
            if (needBreakY)
                break;
        }
        return crops;
    };
};

/** 
 * \brief Poniter will selector preprocessor is config from command line when compiler system
 * \param detectorType is enumarate include Darknet, TensorRT
 * \param gray_frame get frame is gray processing
 * \return detector ptr* pointer algorithm is config from preprocessor
 */
BaseDetector *CreateDetector(detection::Detectors detectorType, const config_t &config, cv::UMat &gray_frame);

#endif /* BASE_DETECTOR_H */
