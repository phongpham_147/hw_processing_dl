/** 
 *  @file   TensorRTDetector.hpp
 *  @brief  Class YoloTensorRTDetecto provide the function API C/C++ using framework Darknet.
 *  @author Pham Thanh Phong 
 *  @date   2020-10-10 
 ***********************************************/

#ifndef TENSOR_RT_DETECTOR_H
#define TENSOT_RT_DETECTOR_H

#include "Detector.hpp"
#include "class_detector.h"

/** 
 * \brief The TensorRTDetector class using framework NVIDIA build algorithm detector. TensorRT help increase performance CPU
 */
class TensorRTDetector : public BaseDetector
{
public:
	TensorRTDetector(const cv::UMat& colorFrame);
	~TensorRTDetector(void);

	/** 
     * \brief Checks whether path to where storage of models .weights and .cfg  has correct, at the
	 * same time it will operation asign as path to as variable storage.
	 * \param config  The command configuration 
	 * \return true if detector is initialize done, false when path to end.
	 */
	bool Init(const config_t& config);

	/** 
	 * \brief Sets the the select when paramater maxCroppRaito less than 0 then function will call process detector with frame original.
	 * Esle then call function detector in crop with paramater crop ratio. 
	 * \param colorFrame metric image
	 */
	void Detect(const cv::UMat& colorFrame);

	/** 
	 * \brief Check whether frame is convert to gray
	 * This is boolean plugin provide for developer easy debug and not show problem after.
	 * \return false
	 */
	bool CanGrayProcessing() const
	{
		return false;
	}

private:
	std::unique_ptr<tensor_rt::Detector> m_detector;

    float m_maxCropRatio = 3.0f;
	std::vector<std::string> m_classNames;
	tensor_rt::Config m_localConfig;
};

#endif /* TENSOT_RT_DETECTOR_H */