/** 
 *  @file   DarknetDetector.hpp
 *  @brief  Class DarknetDetector provide the function API C/C++ using framework Darknet.
 *  @author Pham Thanh Phong 
 *  @date   2020-10-10 
 ***********************************************/

#ifndef DARKNET_DETECTOR_H
#define DARKNET_DETECTOR_H

#include "Detector.hpp"

#include "yolo_v2_class.hpp"

/** 
 * \brief The DarknetDetector class using framework darknet build algorithm detector
 */
class DarknetDetector : public BaseDetector
{
public:
	/** 
	 * \brief The DarknetDetector get frame RGB  provide for framework Darnknet processing object detector
	 * \param colorFrame
	 */
    DarknetDetector(const cv::UMat& colorFrame);
	~DarknetDetector(void);
	
	/** 
     * \brief Checks whether path to where storage of models .weights and .cfg  has correct, at the
	 * same time it will operation asign as path to as variable storage.
	 * \param config  The command configuration 
	 * \return true if detector is initialize done, false when path to end.
	 */
	bool Init(const config_t& config);

	/** 
	 * \brief Sets the the select when paramater maxCroppRaito less than 0 then function will call process detector with frame original.
	 * Esle then call function detector in crop with paramater crop ratio. 
	 * \param colorFrame metric image
	 */
	void Detect(const cv::UMat& colorFrame);

	/** 
	 * \brief Check whether frame is convert to gray
	 * This is boolean plugin provide for developer easy debug and not show problem after.
	 * \return false
	 */
	bool CanGrayProcessing() const
	{
		return false;
	}

private:
	std::unique_ptr<Detector> m_detector;

    float m_confidenceThreshold = 0.5f;
    float m_maxCropRatio = 3.0f;
	std::vector<std::string> m_classNames;
	std::set<std::string> m_classesWhiteList;

	void DetectInCrop(const cv::Mat& colorFrame, const cv::Rect& crop, regions_t& tmpRegions);
	void Detect(const cv::Mat& colorFrame, regions_t& tmpRegions);
	void FillImg(image_t& detImage);

	cv::Mat m_tmpImg;
	std::vector<float> m_tmpBuf;
};

#endif /* DARKNET_DETECTOR_H */