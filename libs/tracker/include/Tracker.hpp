#ifndef CREATE_TRACKER_H
#define CREATE_TRACKER_H

#include <iostream>
#include <vector>
#include <memory>
#include <array>
#include <deque>
#include <numeric>
#include <map>
#include <set>

#include "../../common/defines.hpp"
#include "Track.hpp"
#include "ShortPathCalculator.hpp"

/** 
 * \brief The TrackerSettings structure produce
 */
struct TrackerSettings
{
	tracking::KalmanType m_kalmanType = tracking::KalmanLinear;
	tracking::FilterGoal m_filterGoal = tracking::FilterCenter;
	tracking::LostTrackType m_lostTrackType = tracking::TrackKCF;
	tracking::MatchType m_matchType = tracking::MatchHungrian;

	std::array<track_t, tracking::DistancesCount> m_distanceType;

	/** 
	 * \brief Time step for Kalman
	 */
	track_t m_dateTime = 1.0f;

	/** 
	 * \brief Argument Noise magnitude for Kalman 
	 */
	track_t m_accelNoiseMagnitude = 0.1f;

	/** 
	 * \brief Constant velocity or constant acceleration motion model
	 */
	bool m_useAcceleration = false;

	/** 
	 * \brief Distance threshols for assigment probblem : (from 0 to 1)
	 * The `m_distanceThreshold` is parameter specifies the maximum allowable threshold for the distance between the object and the track.
	 */
	track_t m_distanceThreshold = 0.8f;

	/** 
	 * \brief Minimal area radius in pixels for object centers 
	 * The m_minAreaRadiusPix parameter sets the minimum values ​​in pixels for the diagonals of the ellipse used to calculate the distances D(c) and D(R). This limitation is necessary for stationary or slow objects, so that they can be associated with detections during noisy detection. 
	 * 
	 * For example, you can assume that the object can be displaced by 1 frame by 1/20 of the frame width, then m_minAreaRadius = frameWidth / 20. 
	 * This value is used if it is greater than or equal to 0. 
	 */
	track_t m_minAreaRadiusPix = 20.f;

	/** 
	 * \brief Minimal area radius in ration for object size. Used if `m_minAreaRadiusPix < 0`
	 * 
	 * The m_minAreaRadiusK parameter , as well as m_minAreaRadiusPix, sets the minimum values ​​for the diagonals of the ellipse, but sets not in pixels, but in proportions to the size of the object. 
	 * This can be useful when orienting the camera towards the horizon, where objects (for example, cars) can move away into the distance, gradually decreasing, and also drive up close to the camera. 
	 * In this case, the constant parameter m_minAreaRadiusPix will not work adequately only in one place of the frame. m_minAreaRadiusK is used if m_minAreaRadiusK <0.
	 */
	track_t m_minAreaRadiusK = 0.5f;

	/** 
	 * \brief If the object don't assignment more than this frames then it will be removed
	 * 
	 * The `m_maximumAllowedSkippedFrames` parameter specifies the lifetime of the track without detections, that is, the number of consecutive frames where the track is not associated with any detected object. 
	 * If the number of frames exceeds this threshold, then the track is deleted
	 */
	size_t m_maximumAllowedSkippedFrames = 25;

	/** 
	 * \brief The naximum trajectoryy length
	 * 
	 * The `m_maxTraceLength` parameter is responsible for the maximum length of the track trajectory that is saved and displayed on the screen. 
	 * If this value is exceeded, the earlier points of the path are replaced by the later ones.
	 */
	size_t m_maxTraceLength = 50;

	/** 
	 * \brief Detection abandoned objects 
	 */
	bool m_useAbandonedDetection = false;

	/** 
	 * \brief After this time (in seconds) the objecr is considered abandoned
	 */
	int m_minStaticTime = 5;

	/** 
	 * \brief After this time (in second) the abandoned object will be removed
	 */
	int m_maxStaticTime = 25;

	/** 
	 * \brief Object types that can be matched while tracking
	 */
	std::map<std::string, std::set<std::string>> m_nearTypes;

	/** 
	 * \brief The TrackerSettings constructor
	 */
	TrackerSettings()
	{
		m_distanceType[tracking::DistanceCenters] = 0.0f;
		m_distanceType[tracking::DistanceRectangles] = 0.0f;
		m_distanceType[tracking::DistanceJaccard] = 0.5f;
		m_distanceType[tracking::DistanceHist] = 0.5f;

		assert(CheckDistance());
	}

	/** 
	 * \brief The CheckDistance 
	 */
	bool CheckDistance() const
	{
		track_t sum = std::accumulate(m_distanceType.begin(), m_distanceType.end(), 0.0f);
		track_t maxOne = std::max(1.0f, std::fabs(sum));
		return std::fabs(sum - 1.0f) <= std::numeric_limits<track_t>::epsilon() * maxOne;
	}

	/** 
	 * \brief The SetDistances method is used to share the types of distances listed above. They can be combined with each other by using together, for example DistCenters and DistHist. 
	 * In this case, the total distance between objects will be a linear combination of the specified distances.
	 * In this case, it is necessary that the sum of the coefficients of the linear combination is equal to 1.
	 * 
	 * \param distType Distance of object
	 */
	bool SetDistances(std::array<track_t, tracking::DistancesCount> distType)
	{
		bool res = true;
		auto oldDistances = m_distanceType;
		m_distanceType = distType;
		if (!CheckDistance())
		{
			m_distanceType = oldDistances;
			res = false;
		}
		return res;
	}

	/** 
	 * \brief  The SetDistance method sets the method for calculating the distance between tracked tracks and objects found on the current frame.
	 * \param distType 
	 * \return true 
	 */
	bool SetDistance(tracking::DistanceType distType)
	{
		std::fill(m_distanceType.begin(), m_distanceType.end(), 0.0f);
		m_distanceType[distType] = 1.f;
		return true;
	}

	/** 
	 * \brief The method allows you to define correspondences between different types of objects that are visually similar to each other, but are different. 
	 * The detector can confuse these types of objects with each other (defined in the confusion matrix when training the detector), therefore they will be tracked as different objects. 
	 * 
	 * For example, the types “person” and “child”, “truck” and “bus”, “motorcyclist” and “cyclist” and others are often confused.
	 * Such types can be "linked" to each other using this method and they will be matched when tracking. 
	 * The last parameter of the method indicates that the relationship between types can be symmetric (object1 can map to object2 and vice versa) or asymmetric (object1 can map to object2, but object2 cannot map to object1).
	 * \param type1 
	 * \param type2
	 * \param sym symetric 
	 */
	void AddNearTypes(const std::string &type1, const std::string &type2, bool sym)
	{
		auto AddOne = [&](const std::string &type1, const std::string &type2) {
			auto it = m_nearTypes.find(type1);
			if (it == std::end(m_nearTypes))
			{
				m_nearTypes[type1] = std::set<std::string>{type2};
			}
			else
			{
				it->second.insert(type2);
			}
		};
		AddOne(type1, type2);
		if (sym)
		{
			AddOne(type2, type1);
		}
	}

	/** 
	 * \brief Check whether types have is empty or 2 type use has as the same 
	 * \param type1 String type distances type using
	 * \param type2 
	 * \return 
	 */
	bool CheckType(const std::string &type1, const std::string &type2) const
	{
		bool flag = type1.empty() || type2.empty() || (type1 == type2);
		if (!flag)
		{
			auto it = m_nearTypes.find(type1);
			if (it != std::end(m_nearTypes))
			{
				flag = it->second.find(type2) != std::end(it->second);
			}
		}
		return flag;
	}
};

/** 
 * \brief When creating a `CTracker` class, you must specify the parameters of all methods and algorithms that will participate in the process of its operation using the TrackerSettings structure.
 *  This structure contains the following configuration options:
 */
class CTracker
{
public:
	CTracker(const TrackerSettings &settings);
	CTracker(const CTracker &) = delete;
	CTracker(CTracker &&) = delete;
	CTracker &operator=(const CTracker &) = delete;
	CTracker &operator=(CTracker &&) = delete;

	~CTracker(void);

	/** 
	 * \brief 
	 * \param regions
	 * \param currentTime
	 * \param fps
	 */
	void Update(const regions_t &regions, cv::UMat currFrame, float fps);

	/** 
	 * \brief CanColorFrameToTrack 
	 * \return color frame
 	 */
	bool CanGrayFrameToTrack() const
	{
		bool needColor = (m_settings.m_lostTrackType == tracking::LostTrackType::TrackGOTURN) ||
						 (m_settings.m_lostTrackType == tracking::LostTrackType::TrackDAT) ||
						 (m_settings.m_lostTrackType == tracking::LostTrackType::TrackSTAPLE) ||
						 (m_settings.m_lostTrackType == tracking::LostTrackType::TrackLDES);
		return !needColor;
	}

	/** @brief CanColorFrameToTrack 
	 * return
	 */

	/** 
	 * \brief 
	 * \return true
	 */
	bool CanColorFrameToTrack() const
	{
		return true;
	}

	/**
	 * \brief
	 * \return size of tracker
	 */
	size_t GetTracksCount() const
	{
		return m_tracks.size();
	}

	/** 
	 * \brief 
	 * \return get object tracker
	 */
	std::vector<TrackingObject> GetTracks() const
	{
		std::vector<TrackingObject> tracks;
		if (!m_tracks.empty())
		{
			tracks.reserve(m_tracks.size());
			for (const auto &track : m_tracks)
			{
				tracks.push_back(track->ConstructObject());
			}
		}
		return tracks;
	}

private:
	TrackerSettings m_settings;

	tracks_t m_tracks;

	size_t m_nextTrackID;

	cv::UMat m_prevFrame;

	std::unique_ptr<ShortPathCalculator> m_SPCalculator;

	void CreateDistaceMatrix(const regions_t &regions, std::vector<RegionEmbedding> &regionEmbeddings, distMatrix_t &costMatrix, track_t maxPossibleCost, track_t &maxCost, cv::UMat currFrame);
	void UpdateTrackingState(const regions_t &regions, cv::UMat currFrame, float fps);
};

#endif /* CREATE_TRACKER_H */