/**
 * @ Author: Pham Thanh Phong
 * @ Create Time: 2020-10-06 23:30:41
 * @ Modified by: Your name
 * @ Modified time: 2020-10-16 20:25:57
 * @ Description:
 */

#ifndef DEFINES_H
#define DEFINES_H

#include <vector>
#include <string>
#include <map>
#include <opencv2/opencv.hpp>

typedef float track_t;
typedef cv::Point_<track_t> Point_t;
typedef std::vector<int> assignments_t;
typedef std::vector<track_t> distMatrix_t;

#define El_t CV_32F /*!< defines the depth of each element of the matrix, while */
#define Mat_t CV_32FC /*!< defines both the depth of each element and the number of channels. */

/** 
 * \brief Storage the variable configuration in multimap provide transmission from CLI to storage source.
 */
typedef std::multimap<std::string, std::string> config_t;

/** 
 * \brief The CRegion class
 */
class CRegion
{
public:
    CRegion()
        : m_type(""), m_confidence(-1)
    {
    }

    CRegion(const cv::Rect &rect)
        : m_brect(rect)
    {
        B2RRect();
    }

    CRegion(const cv::RotatedRect &rrect)
        : m_rrect(rrect)
    {
        R2BRect();
    }

    CRegion(const cv::Rect &rect, const std::string &type, float confidence)
        : m_type(type), m_brect(rect), m_confidence(confidence)
    {
        B2RRect();
    }

    std::string m_type;
    cv::RotatedRect m_rrect;
    cv::Rect m_brect;
    float m_confidence = -1;

private:
    ///
    /// \brief R2BRect
    /// \return
    ///
    cv::Rect R2BRect()
    {
        m_brect = m_rrect.boundingRect();
        return m_brect;
    }
    ///
    /// \brief B2RRect
    /// \return
    ///
    cv::RotatedRect B2RRect()
    {
        m_rrect = cv::RotatedRect(m_brect.tl(), cv::Point2f(static_cast<float>(m_brect.x + m_brect.width), static_cast<float>(m_brect.y)), m_brect.br());
        return m_rrect;
    }
};

typedef std::vector<CRegion> regions_t;

namespace detection
{
    /*! This is enum varibale storage the framework algorithm for model */
    enum Detectors
    {
        Darknet = 0, /*!< This is framework key select using algoirthm Darknet for model */
        TensorRT, /*!< This is framework key select usinh algorithm TensorRT for model */
    };
} // namespace detection

namespace tracking
{
    /*! This is enum variable storage key the algorithm calculator distance   
     * produce for algorithm tracking object
     */
    enum DistanceType
    {
        /** 
         * \brief An ellipse is described around the current track center, the diagonals of which are equal to its triple speed (the speed is obtained from the state matrix of the Kalman filter).
         * Calculates the ratio of the Euclidean distance between the center of the object and the center of the ellipse to the distance from the center to the edge of the ellipse. 
         * If the center of the object coincides with the center of the ellipse, 
         *   then the final value of the distance will be D(c) = 0; 
         * if on the boundary or behind the ellipse, 
         *   then D(c) = 1; in other cases, the distance lies in the interval (0; 1).
         */
        DistanceCenters,

        /** 
         * \brief the final distance D (R) is a weighted sum of three values: the distance from the point above `D(c)`, 
         * the ratio of the width of the object to the width of the track `D(W) = min(W(obj), W(tr)) / max(W(obj), W(tr))` and the ratio of the object height to the track height D(H) = min(H (obj), H(tr)) / max(H(obj), H(tr)). 
         * That is, D (R) = 1 - (1 - D (c)) * (D (W) + D (H))/2`. 
         * Obviously, D(R) also lies on the segment [0; 1]. 
         */
        DistanceRectangles, 

        /** 
         * \brief Jaccard distance or IoU (intersection over union) 
         * The ratio of the intersection area to the area of ​​the union of the object's and track's rectangles, D (J) also lies on the segment [0; 1]. 
         * It makes no sense to use this type of distance when tracking small objects and / or objects that can move more than their size in one frame.
         */
        DistanceJaccard, 

        /** 
         * \brief  Inside the rectangle of the object on the current frame and inside the rectangle of the track on the frame on which it was last detected, 2 histograms are built, which are compared by the Bhattacharya method. 
         * The obtained distance D (Hist) is by definition located on the segment [0; 1]. This distance is computationally the heaviest of all. It makes sense to use it only for objects that are large enough in size, which should differ significantly from each other.
         * In this case, it is worth considering the fact that the circumscribing rectangle can capture part of the background and different objects with the same background can be more similar to each other than one object that has passed from one background to another.
         */
        DistanceHist, 
        
        DistancesCount  
    };

    /*!The m_filterGoal parameter defines what exactly will be contained in the Kalman filter state vector (tracking :: FilterGoal):*/
    enum FilterGoal
    {
        
        /*!< Only the coordinates of the center of the object and the speed of its movement will be filtered. In this case, in the absence of matching the track with detections, its dimensions will be constant, equal to the last detected values. */
        FilterCenter, 
        /*!< The size of the object will also be filtered, they are included in the transition matrix. An important difference in this case is that the filter memorizes the rate at which the object changes its size: if it was removed and no longer detected, then the track size will decrease at the same rate (linear). The problem with this filter lies precisely in the linearity of its operation: when an object approaches the camera or moves away from it to the horizon, the size of a real object changes nonlinearly. If the object is not detected for several frames, then the track size will change too much in relation to its real size. Therefore, this type of filtering is not suitable for all types of video camera installations.  */
        FilterRect
    };

    /* This is enum */
    enum KalmanType
    {
        KalmanLinear,
        KalmanUnscented,
        KalmanAugmentedUnscented
    };

    /*! This is enum  */
    enum MatchType
    {
        MatchHungrian,
        MatchBipart
    };

    /** 
     * \brief The m_lostTrackType parameter defines the way the track will predict its position on the frame if it has not been associated with any detected object. 
     * In this case, the track memorizes the coordinates of the object in the frame where it was last detected, and searches for its new position on subsequent frames without detections. 
     * Searching for an object in the frame occurs with each loss of an object and can create a serious computational load. 
     * Search method - visual by picture (VOT, visual object tracking) can take one of the following values ​​(tracking :: LostTrackType): 
     */
    enum LostTrackType
    {
        /** 
         * \brief do not use search, the position on the frame is determined solely by the prediction of the Kalman filter.
         */
        TrackNone,

        /** 
         * \brief using the cv :: TrackerKCF algorithm, quite fast and reasonably accurate to the level of 2014. Able to adapt to changes in object size.
         */
        TrackKCF,

        /** 
         * \brief using the cv :: TrackerMIL algorithm, fast enough but not accurate.
         */
        TrackMIL,

        /** 
         * \brief using cv :: TrackerMedianFlow algorithm, slow.
         */        
        TrackMedianFlow,

        /** 
         * \brief using the cv :: TrackerGOTURN algorithm. The neural network method is very slow on the CPU - there is no point in using it.
         */
        TrackGOTURN,

        /** 
         * \brief using the cv :: TrackerMOSSE algorithm, quite fast and moderately accurate, somewhere at the KCF level.
         */
        TrackMOSSE,

        /** 
         * \brief using the cv :: TrackerCSRT algorithm, slightly slower than KCF, but much more robust. He was the best in the VOT challenge a few years ago, then completely yielding superiority to Siamese neural networks.
         */
        TrackCSRT,

        /** 
         * \brief using the built-in DAT algorithm, worse than CSRT in all respects.
         */
        TrackDAT,

        /** 
         * \brief using the built-in STAPLE algorithm, which in its parameters is closer to CSRT, but can be more accurate due to manual discarding of frames where the object was found with low confidence. Implementation for x86 architecture only.
         */
        TrackSTAPLE,

        /** 
         * \brief using the built-in LDES algorithm, the main difference of which is the detection of the angle of inclination of the object (cv :: RotatedRect). Implementation for x86 architecture only.
         */
        TrackLDES
    };
} // namespace tracking

#endif /* DEFINES_H */