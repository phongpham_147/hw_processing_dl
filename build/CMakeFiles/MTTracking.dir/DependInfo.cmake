# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/phong/Developer/todo/hw_jetson_processing/src/main.cpp" "/home/phong/Developer/todo/hw_jetson_processing/build/CMakeFiles/MTTracking.dir/src/main.cpp.o"
  "/home/phong/Developer/todo/hw_jetson_processing/src/traffic_processing.cpp" "/home/phong/Developer/todo/hw_jetson_processing/build/CMakeFiles/MTTracking.dir/src/traffic_processing.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../libs"
  "../include"
  "/usr/local/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/phong/Developer/todo/hw_jetson_processing/build/libs/tracker/CMakeFiles/tracker.dir/DependInfo.cmake"
  "/home/phong/Developer/todo/hw_jetson_processing/build/libs/detector/CMakeFiles/detector.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
