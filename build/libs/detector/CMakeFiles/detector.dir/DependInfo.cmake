# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/phong/Developer/todo/hw_jetson_processing/libs/detector/base_detector.cpp" "/home/phong/Developer/todo/hw_jetson_processing/build/libs/detector/CMakeFiles/detector.dir/base_detector.cpp.o"
  "/home/phong/Developer/todo/hw_jetson_processing/libs/detector/darknet_detector.cpp" "/home/phong/Developer/todo/hw_jetson_processing/build/libs/detector/CMakeFiles/detector.dir/darknet_detector.cpp.o"
  "/home/phong/Developer/todo/hw_jetson_processing/libs/detector/ocv_dnn_detector.cpp" "/home/phong/Developer/todo/hw_jetson_processing/build/libs/detector/CMakeFiles/detector.dir/ocv_dnn_detector.cpp.o"
  "/home/phong/Developer/todo/hw_jetson_processing/libs/detector/ssd_detector.cpp" "/home/phong/Developer/todo/hw_jetson_processing/build/libs/detector/CMakeFiles/detector.dir/ssd_detector.cpp.o"
  "/home/phong/Developer/todo/hw_jetson_processing/libs/detector/tensorrt_detector.cpp" "/home/phong/Developer/todo/hw_jetson_processing/build/libs/detector/CMakeFiles/detector.dir/tensorrt_detector.cpp.o"
  "/home/phong/Developer/todo/hw_jetson_processing/libs/detector/yolo_detector.cpp" "/home/phong/Developer/todo/hw_jetson_processing/build/libs/detector/CMakeFiles/detector.dir/yolo_detector.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "detector_EXPORTS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../libs"
  "../include"
  "../libs/detector"
  "../libs/detector/../libs"
  "../libs/detector/../common"
  "/usr/local/include/opencv4"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
