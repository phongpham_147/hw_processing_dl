file(REMOVE_RECURSE
  "../../libdetector.pdb"
  "../../libdetector.so"
  "CMakeFiles/detector.dir/base_detector.cpp.o"
  "CMakeFiles/detector.dir/darknet_detector.cpp.o"
  "CMakeFiles/detector.dir/ocv_dnn_detector.cpp.o"
  "CMakeFiles/detector.dir/ssd_detector.cpp.o"
  "CMakeFiles/detector.dir/tensorrt_detector.cpp.o"
  "CMakeFiles/detector.dir/yolo_detector.cpp.o"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/detector.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
